#!/bin/sh

set -e
set -x

dnf debuginfo-install -y kernel

# on Fedora CI, can only assume about 1GB RAM to run these tests

stap -L 'process("stap").mark("*")' | grep pass
# stap -L 'kernel.trace("sys_enter")'
stap -v --example helloworld.stp
stap -v -T 10 -p4 -e 'probe kernel.function("do_exit") {println($$vars)}'
# stap -v -T 10 -e 'probe syscall.* ? {println(argstr)}'

echo
echo RESULT: PASS
